﻿using Ganss.XSS;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TranslationCachingApp
{
    public static class SanitizeHelper
    {
        /// <summary>
        /// This list contains the allowed values for the class attribute.
        /// The value must match exactly (case insensitive) one of the values in this list
        /// to not be removed during sanitization.
        /// </summary>
        private static readonly IReadOnlyList<string> AllowedClasses = new List<string>()
        {
            "nonsymbola", "eq-latex-code", "mathquill-rendered-math eqbtn", "binary-operator", "unary-operator", "florin",
            "fraction", "numerator", "denominator", "sqrt-prefix", "sqrt-stem", "nthroot", "paren", "array",
        };

        /// <summary>
        /// This list contains the allowed values for the id attribute.
        /// This value must start with (case insensitive) one of the values in this list
        /// to not be removed during sanitization.
        /// </summary>
        private static readonly IReadOnlyList<string> AllowedIds = new List<string>() { "mequation" };

        private static HtmlSanitizer HtmlSanitizer
        {
            get
            {
                var sanitizer = new HtmlSanitizer();
                sanitizer.AllowedTags.Remove("form");
                sanitizer.AllowedTags.Remove("input");
                sanitizer.AllowedTags.Remove("output");
                sanitizer.AllowedTags.Remove("button");
                sanitizer.AllowedTags.Remove("select");
                sanitizer.AllowedTags.Remove("option");
                sanitizer.AllowedTags.Remove("textarea");
                sanitizer.AllowedTags.Remove("fieldset");
                sanitizer.AllowedTags.Remove("keygen");
                sanitizer.AllowedTags.Remove("a");
                sanitizer.AllowedTags.Remove("img");

                // Dont allow any data-* attributes
                sanitizer.AllowDataAttributes = false;

                // Dont allow any css properties
                sanitizer.AllowedCssProperties.Clear();

                sanitizer.RemovingAttribute += sanitizer_RemovingAttribute;
                return sanitizer;
            }
        }

        private static void sanitizer_RemovingAttribute(object sender, RemovingAttributeEventArgs e)
        {
            if (string.Equals(e.Attribute.Name, "id", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("In Sanitize Function: " + e.Attribute.Name);
                var attributeValue = e.Attribute.Value.ToLower();
                if (AllowedIds.Any(id => attributeValue.StartsWith(id)))
                {
                    e.Cancel = true;
                }
            }
            if (string.Equals(e.Attribute.Name, "class", StringComparison.OrdinalIgnoreCase))
            {
                if (AllowedClasses.Contains(e.Attribute.Value.ToLower()))
                {
                    e.Cancel = true;
                }
            }
        }

        public static string Sanitize(this string node)
        {

            if (node != null)
            {
                node = HtmlSanitizer.Sanitize(node);
            }

            return node;
        }

        public static ICollection<string> Sanitize(this ICollection<string> nodes)
        {
            if (nodes != null)
            {
                var sanitizer = HtmlSanitizer;

                nodes = nodes.Select(x => sanitizer.Sanitize(x)).ToList();

            }

            return nodes;
        }


    }
}
