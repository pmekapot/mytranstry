﻿using System;
using System.Collections.Generic;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using System.Linq;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializerAttribute(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace TranslationCachingApp
{
    public class Storage
    {
        public Dictionary<int, string> input = new Dictionary<int, string>();
        public Dictionary<int, int> forwardIndex = new Dictionary<int, int>();
        public Dictionary<int, int> reverseIndex = new Dictionary<int, int>();
        public Dictionary<int, string> output = new Dictionary<int, string>();
    }

    public class ProcessRequest    
    {
        private static AmazonDynamoDBClient client = new AmazonDynamoDBClient();        // DynamoDB connection 
        private static string tableName = "Translations";                               // Table Name in Dynamodb -- Fixed 
        private static string exceptionString = "";                                     // String to return message if exception occurs


        /*
         * setTransDynamo : For caching data to DynamoDB
         * 
         * Parameters : Composed String
         *              Translated Text
         * Return : void
         */
        public async static void setTransDynamo(string composedstr, string tranText)
        {
            try
            {
                Table transCatalog = Table.LoadTable(client, tableName);         //Loading table
                Console.WriteLine("\n Executing setTransDynamo method");
                Console.WriteLine("\n ComposedStr and Translated Text : " + composedstr + "  " + tranText);
                var trans = new Document();                                     // Adding new document to DynamoDB Table
                trans["source"] = composedstr;
                trans["target"] = tranText;
                await transCatalog.PutItemAsync(trans);                         // Document adding to Table
            }
            catch(Exception e)
            {
                Console.WriteLine("\n There is an Exception while connecting to DynamoDB:  " + e);   //actual exception written to cloudwatch logs
                exceptionString = "Exception occured while connecting to DynamoDB. Please contact Administrator";  // Dummy message to user
                throw new Exception("Exception occured while connecting to DynamoDB:  ");
            }
        }


        /*
         * getTransDynamo : For getting cached data from DynamoDB
         * 
         * Parameters : Source language
         *              target language
         *              text to be translated
         * Return : Task<String> -- translated text if match found else returns null
         */
        public async static Task<string> getTransDynamo(string source, string target, string text)
        {
            string ret = null;
            /*
             * Composed String is formed as source_target-text 
             * Example: en_es-hello 
             */
            string composedSource = source + "_" + target + "-" + text;     
            Console.WriteLine("composedSource : "+composedSource);

            try
            {
                Table transCatalog = Table.LoadTable(client, tableName);
                Console.WriteLine("\n Executing getTransDynamo method");

                GetItemOperationConfig config = new GetItemOperationConfig         
                {
                    AttributesToGet = new List<string> { "target" },
                    ConsistentRead = true
                };

                Console.WriteLine("\n Executing getTransDynamo before getitemasync");
                Document transDocument = await transCatalog.GetItemAsync(composedSource);    //Getting target value from table for composed string
                if (transDocument != null)
                {
                    foreach (var attribute in transDocument.GetAttributeNames())
                    {
                        string stringValue = null;
                        var value = transDocument[attribute];
                        if (value is Primitive)
                        {
                            stringValue = value.AsPrimitive().Value.ToString();
                            ret = stringValue;
                        }
                        else if (value is PrimitiveList)                                    //For future if we DynamoDB target column is list
                        {
                            stringValue = string.Join(",", (from primitive
                                            in value.AsPrimitiveList().Entries
                                                            select primitive.Value).ToArray());
                            Console.WriteLine("{0} - {1}", attribute, stringValue);
                            Console.WriteLine("Printing: ", stringValue);
                            ret = stringValue;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Target does not exist in DynamoDB");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("\n There is an Exception while connecting to DynamoDB:   " + e);                 //actual exception written to cloudwatch logs
                exceptionString = "Exception occured while connecting to DynamoDB. Please contact Administrator";   // Dummy message to user
                throw new Exception("Exception occured while connecting to DynamoDB:  ");
            }
            return ret;
        }


        /*
         * GetParms : For getting Query Parameters from the URL
         * 
         * Parameters : Query parameters dictionary
         *              parameter name
         *              
         * Return : String 
         */
        private string GetParms(IDictionary<string, string> qsParms, string parmName)
        {
            var result = string.Empty;
            if (qsParms != null)
            {
                if (qsParms.ContainsKey(parmName))
                {
                    result = qsParms[parmName];
                }
            }
            return result;
        }

        public APIGatewayProxyResponse Get(APIGatewayProxyRequest request, ILambdaContext context)
           {
            context.Logger.LogLine("*************Serverless TranslationCaching App ************\n");
            context.Logger.LogLine("request.QueryStringParameters: "+ request.QueryStringParameters);

            string source = GetParms(request.QueryStringParameters, "source");
            string target = GetParms(request.QueryStringParameters, "target");
           
            string query = GetParms(request.QueryStringParameters, "q");
            string queryArrStr = query.ToString();
            context.Logger.LogLine("queryArray as String:  \n" + queryArrStr);
            string[] queryArray = query.Split('|');                                 // Separator : '|'  (%7C)

            context.Logger.LogLine("queryArray Length:  \n" + queryArray.Length);
            for (int ii=0; ii< queryArray.Length-1; ii++)     
            { 
            context.Logger.LogLine("queryArray contents:  \n" + queryArray[ii]);   // Printing to Cloudwatch logs
            }

            string callback = GetParms(request.QueryStringParameters, "callback");

            string googleRequest = "https://www.googleapis.com/language/translate/v2?key=AIzaSyDKBKUKEbWIfBMOcIwmyiQIriBku8Ns090";
       
            int translateCount = 0;
            Storage storage = new Storage();
            string[] decodedArray = new String[2000];   // Buffer to store decoded text
            int dCount = 0;

            try
            {
                for (int i = 0; i < queryArray.Length-1; i++) //ignoring last array item as it will be null
                {
                    storage.input[i] = queryArray[i];
                    string value = queryArray[i];
                    context.Logger.LogLine("Value initial:  \n" + value);
                    value = value.Sanitize();

                    var decodedValue = WebUtility.UrlDecode(value);
                    context.Logger.LogLine("Sanitize Value:  \n" + value);
                    context.Logger.LogLine("decoded Value:  \n" + decodedValue);
                    Task<string> ts = getTransDynamo(source, target, decodedValue);     //Calling getTransDynamo 
                    string targetText = ts.Result;
                    context.Logger.LogLine("targetText Value:  \n" + targetText);

                    if (targetText == null)
                    {
                        googleRequest += "&q=" + decodedValue;
                        storage.forwardIndex[translateCount] = i;
                        storage.reverseIndex[i] = translateCount;
                        translateCount++;
                        context.Logger.LogLine("TargetText Added:  \n" + translateCount);
                        decodedArray[dCount] = decodedValue;
                        dCount++;
                    }
                    else
                    {
                        storage.output[i] = targetText;
                        context.Logger.LogLine("TargetText Found in DynamoDB \n");
                    }

                }


                //googleRequest += "&source=en";
                //googleRequest += "&target=fr";
                //googleRequest += "&callback=gh.ultraLight.utils.dynamicCallback.cb1324518685662";
                //googleRequest = "https://www.googleapis.com/language/translate/v2?key=AIzaSyDKBKUKEbWIfBMOcIwmyiQIriBku8Ns090&q=This+is+of+the+emergency+taco+system.&source=en&target=fr&callback=gh.ultraLight.utils.dynamicCallback.cb1324518685662";

            //  googleRequest = googleRequest + "&source=" + source + "&target=" + target + "&callback=" + callback;

                googleRequest = googleRequest + "&source=" + source + "&target=" + target; //removed callback -- working

                context.Logger.LogLine("googleRequest URL formed:  \n" + googleRequest);

                context.Logger.LogLine("storage.forward.Count:  \n" + storage.forwardIndex.Count);

                String result;
                try
                {
                    if (storage.forwardIndex.Count > 0)
                    {
                        var watch = System.Diagnostics.Stopwatch.StartNew();            //To check google api call time -- debug purpose

                        result = Apicall(googleRequest);                                // Calling Apicall method
                        watch.Stop();
                        var elapsedMs = watch.ElapsedMilliseconds;
                        context.Logger.LogLine("Google API call Time: \t " + elapsedMs);


                        Dictionary<string, object> jsonObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);

                        Console.WriteLine("Returned JsonObject: " + jsonObject);       //jsonObject returned

                        dynamic data = jsonObject["data"];
                        dynamic translations = data["translations"];
                        for (int i = 0; i < translations.Count; i++)
                        {
                            Console.WriteLine("Deserialization Count: " + translations.Count);

                            string translatedText = translations[i]["translatedText"];

                            Console.WriteLine("Deserialization Translated Text: " + translatedText);

                            storage.output[storage.forwardIndex[i]] = translatedText;

                            Console.WriteLine("Deserialization storage.output: " + storage.output[storage.forwardIndex[i]]);

                            string composedStr = source + "_" + target + "-" + decodedArray[i];

                            Console.WriteLine("Deserialization Composed String: " + composedStr);

                            setTransDynamo(composedStr, translatedText); //caching in DynamoDB

                            Console.WriteLine("Deserialization i/p o/p: " + storage.input[storage.forwardIndex[i]] + " " + translatedText);
                        }
                    }
                }
                catch (HttpRequestException hre)
                {
                    result = "Server unreachable";
                    context.Logger.LogLine("HttpRequestException:  \n" + hre);
                }

                string returnResult = "";

                if (callback != "")
                {
                    returnResult += callback + "(";
                }

                returnResult += "{ \"data\": { \"translations\": [";
                foreach (KeyValuePair<int, string> item in storage.output.OrderBy(key => key.Key))
                {
                    returnResult += "{ \"translatedText\" : \"" + item.Value + "\" },";
                }
                returnResult = returnResult.TrimEnd(',');
                returnResult += "]}}";

                if (callback != "")
                {
                    returnResult += ");";
                }

                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = returnResult,
                    Headers = new Dictionary<string, string> { { "Content-Type", "application/json" }, { "Access-Control-Allow-Origin", "*" } }
                };
                return response;
            }
            catch
            {
                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Body = exceptionString,
                    Headers = new Dictionary<string, string> { { "Content-Type", "application/json" }, { "Access-Control-Allow-Origin", "*" } }
                };
                return response;
            }

     }

        /*
         * Apicall : For calling URL and returning response
         * 
         * Parameters : URL string
         *              
         * Return : String 
         */
        public static string Apicall(string url)
             {
            using (var client = new HttpClient { Timeout = TimeSpan.FromSeconds(115)})
            {
                try
                {
                    string stringResult = "";
                    client.DefaultRequestHeaders.Add("Referer", "learn.edgenuity.com");
                    var response = client.GetAsync(url).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = response.Content;
                            stringResult = responseContent.ReadAsStringAsync().Result;

                            Console.WriteLine(stringResult);
                        }

                    if(stringResult.Contains("badRequest"))
                    {
                        throw new HttpRequestException();
                    }                    
                    return stringResult;
               }

                catch (HttpRequestException httpRequestException)
                {
                    Console.WriteLine("Response ApiCall Exception: " + httpRequestException);
                    exceptionString = "Exception Occured calling Google API. Please contact Administrator";
                    throw new Exception("Exception Occured calling Google API ");
                }
            }
        }
    }
}
